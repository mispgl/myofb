#!/bin/sh

curl -f https://s3.amazonaws.com/lists.disconnect.me/simple_ad.txt |
    sed -E -e 's/^\s*#.*$//' -e '/^$/d'
