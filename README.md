# MYOFB - Mind Your Own Fucking Business

Minimal blocking system for ads, trackers, evil sites, etc.

This project is the home-made unix-friendly version of some more
not-invented-here and user-friendly projects like [pi-hole](https://pi-hole.net/).


## Usage

Whlie manual pages are being written, the short version of this is:

1. Install it:

    root# make install

2. Configure it with sources:

    root# myofb-config

Myofb consults a list of sources, constructed from the
[firebog.net](https://firebog.net/) collection.  The `myofb-config` command
allows to enable all or some sources.

3. (Optional) define a whitelist

List in `/etc/myofb/whitelist` the domains you always want to connect to.

4. Use it.

    user$ myofb-sync

The `myofb-sync` command will emit on stdout a black-list of hosts according
to your preferences.  The list can be very long!

The intended use is to pipe it into a filter to transform it in some useful
format:

    root# myofb-sync | myofb-to-hosts > /etc/hosts.filter

The `myofb-sync` command will emit a diff with the previous run on stderr (a
cache is stored in `/var/cache/myofb/` for this to happen).  The intended
use for this is to receive local mails with blacklists updates from a
cron-job:

    root# crontab -l
    SHELL=/bin/sh
    PATH=/usr/local/bin:/usr/bin/

    0 12 * * * myofb-sync | myofb-to-hosts > /etc/hosts.filter

## Limitations

This project is in working state, but it is still experimental.  Manual
pages are still lacking (even though the code is extremely simple).

The most important part is that it lacks of some automated way of syncing up
with the firebog list of lists!  So if a new list is added, myofb needs to
be manually updted.


## Installation

System wide installation with prefix "/usr/local" can be done with

    make install

Installation with a different prefix can be done as follows:

    make install prefix=/some/local/path

Installation for packaging is also possible: the DESTDIR convention is
honored:

    make install prefix=/usr DESTDIR=%{buildroot}

## Documentation

FUTURE: See manual pages in the "doc" directory.
CURRENTLY: not ready

## TODO

- myofb-sync
 - "-q" mode (quiet, no patch on stderr)
